<?php
include_once __DIR__ . '/includes/globals.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
    <title>ToDo List</title>
</head>
<body class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Lista Todo</a>
  </div>
</nav>

<main class="mt-3">
<?php
if (isset($_GET['stato']) && isset($_GET['messages'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}
if (isset($_GET['action']) && $_GET['action'] === 'loadtodo' && isset($_GET['id'])) {
    $todos = \DataHandling\Todo::selectData($_GET['id']);
}
?>
<?php if (isset($todos[0])) : ?>
<form action="/todolist/includes/updateTodo.php" method="post">
<input type="hidden" name="id" id="id" value="<?php echo $todos[0]['id']; ?>">
<?php else : ?>
  <form action="/todolist/includes/addTodo.php" method="post">
<?php endif;?>
<div class="row">
    <div class="col-8">
    <input class="form-control" type="text" name="testo" id="testo"
      value="<?php echo isset($todos[0]) ? $todos[0]['testo'] : '' ?>" placeholder="Inserisci il Testo del TODO">
    </div>
    <div class="col-2">
    <input type="submit" class="btn btn-primary w-100"
      value="<?php echo isset($todos[0]) ? 'Modifica Todo (' . $todos[0]['id'] . ')' : 'Aggiungi' ?>">
    </div>
    <div class="col-2">
    <a href="/todolist/index.php" type="submit" class="btn btn-light w-100">RESET</a>
    </div>
</div>
</form>
<?php $todosList = \DataHandling\Todo::selectData();
if (count($todosList) > 0) :
    ?>
<div class="row mt-3">
  <div class="col-4">
  <a href="/todolist/includes/updateTodo.php?id=tutti&stato=0"
    class="btn btn-outline-danger w-100">Rendi Tutti Non Completati</a>
  </div>
  <div class="col-4">
  <a href="/todolist/includes/updateTodo.php?id=tutti&stato=1"
    class="btn btn-outline-success w-100">Rendi Tutti Completati</a>
  </div>
  <div class="col-4">
  <a href="/todolist/includes/delTodo.php?id=tutti" class="btn btn-danger w-100">Elimina tutti i Todo</a>
  </div>
</div>
<table class="mt-3 table table-striped table-hover table-bordered table-responsive">
  <thead>
    <?php echo \DataHandling\Utils\get_table_head($todosList[0]); ?>
  </thead>
  <tbody>
    <?php echo \DataHandling\Utils\get_table_body($todosList); ?>
  </tbody>
</table>
<?php else : ?>
  <p class="mt-3 alert alert-dark" role="alert">Nessun Todo da mostrare, aggiungine uno!</p>
<?php endif;?>
</main>
</body>
</html>
