<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

abstract class FormHandle
{
    use \DataHandling\Utils\InputSanitize;
    abstract protected static function sanitize($fields);
    abstract public static function insertData($form_data);
    abstract public static function selectData($idTodo = null);
    abstract public static function deleteData($id);
    abstract public static function updateData($form_data);
}
