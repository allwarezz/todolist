<?php
namespace DataHandling;

class Todo extends FormHandle
{

    protected static function sanitize($fields)
    {
        $errors = array();
        $fields['testo'] = self::cleanInput($fields['testo']);
        return $fields;
    }

    public static function insertData($form_data)
    {

        $fields = array(
            'testo' => $form_data['testo'],
        );
        if($fields['testo'] === ''){
            header('Location: http://localhost/todolist/index.php?stato=ko'
            .'&messages=Non è possibile inserire un todo vuoto');
            exit;
        }
        $fields = self::sanitize($fields);
        $mysqli = \DBHandle\getConnection();

        $query = $mysqli->prepare('INSERT INTO todos (testo) VALUES (?)');
        $query->bind_param('s', $fields['testo']);
        $query->execute();

        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error'] . "\n", 3, 'my-errors.log');
            header('Location: http://localhost/todolist/index.php?stato=ko'
                . '&messages=Non è stato possibile inserire il todo, riprova più tardi');
            exit;
        }
        $query->close();

        header('Location: http://localhost/todolist/index.php?stato=ok&messages=Todo inserito con successo');
        exit;
    }

    public static function selectData($idTodo = null)
    {
        $mysqli = \DBHandle\getConnection();

        if (isset($idTodo)) {
            try {
                $idTodo = intval($idTodo);
                $query = $mysqli->prepare('SELECT * FROM todos WHERE id = ?');
                if (is_bool($query)) {
                    throw new Exception('Query non valida. $mysqli->prepare ha restituito false.');
                }
                $query->bind_param('i', $idTodo);
                $query->execute();
                $query = $query->get_result();
            } catch (Exception $e) {
                error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
            }
        } else {
            $query = $mysqli->query('SELECT * FROM todos ORDER BY stato, dataCreazione DESC');
        }

        $results = array();

        while ($row = $query->fetch_assoc()) {
            $row['testo'] = stripslashes($row['testo']);
            $results[] = $row;
        }

        return $results;
    }

    public static function deleteData($id)
    {
        $mysqli = \DBHandle\getConnection();
        $messages = '';
        if ($id === 'tutti') {
            $result = $query = $mysqli->query('DELETE FROM todos');
            if ($result) {
                header('Location: http://localhost/todolist/?stato=ok&messages=Tutti i Todo sono stati eliminati');
            } else {
                error_log("Errore MySql Nella cancellazione di tutti i Todo" . "\n", 3, 'my-errors.log');
                header('Location: http://localhost/todolist/?stato=ko'
                    . '&messages=Ops, Non è stato possibile rimuovere i Todo');
            }
        } else {
            $id = intval($id);
            try {
                $query = $mysqli->prepare('DELETE FROM todos WHERE id = ?');
                if (is_bool($query)) {
                    throw new Exception('Query non valida. $mysqli->prepare ha restituito false.');
                }
                $query->bind_param('i', $id);
                $query->execute();
            } catch (Exception $e) {
                error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                header('Location: http://localhost/todolist/?stato=ko'
                    . '&messages=Ops, Non è stato possibile rimuovere il Todo');
                exit;
            }
            if ($query->affected_rows > 0) {
                header('Location: http://localhost/todolist/?stato=ok&messages=Todo Eliminato Correttamente');
            } else {
                header('Location: http://localhost/todolist/?stato=ko'
                    . '&messages=Ops, Non è stato possibile rimuovere il Todo');
            }
        }
        exit;
    }

    public static function updateData($form_data)
    {
        if ($form_data['id'] === 'tutti') {
            $mysqli = \DBHandle\getConnection();
            $stato_todos = ($form_data['stato'] == 1) ? 1 : 0;
            try {
                $query = $mysqli->prepare('UPDATE todos SET stato = ?');
                if (is_bool($query)) {
                    throw new Exception('Query non valida. $mysqli->prepare ha restituito false.');
                }
                $query->bind_param('i', $stato_todos);
                $query->execute();
            } catch (Exception $e) {
                error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                header('Location: http://localhost/todolist/?stato=ko'
                    . '&messages=Ops, Non è stato possibile Cambiare stato ai Todo');
                exit;
            }
            header('Location: http://localhost/todolist/index.php?stato=ok'
                . '&messages=Tutti i Todo sono stati aggiornati');
            exit;
        } else {
            $sql = '';
            $id = intval($form_data['id']);
            $mysqli = \DBHandle\getConnection();
            if (!isset($form_data['stato']) && isset($form_data['testo'])) {
                $fields = array(
                    'testo' => $form_data['testo'],
                );
                if($fields['testo'] === ''){
                    header('Location: http://localhost/todolist/index.php?stato=ko'
                    .'&messages=Non è possibile inserire un todo vuoto');
                    exit;
                }
                $fields = self::sanitize($fields);
                try {
                    $query = $mysqli->prepare('UPDATE todos SET testo = ? WHERE id = ?');
                    $query->bind_param('si', $fields['testo'], $id);
                } catch (Exception $e) {
                    error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                    header('Location: http://localhost/todolist/?stato=ko'
                        . '&messages=Ops, Non è stato possibile Aggiornare il Todo');
                    exit;
                }
            } else {
                $stato_todos = ($form_data['stato'] == 1) ? 1 : 0;
                try {
                    $query = $mysqli->prepare('UPDATE todos SET stato = ? WHERE id = ?');
                    $query->bind_param('ii', $stato_todos, $id);
                } catch (Exception $e) {
                    error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                    header('Location: http://localhost/todolist/?stato=ko'
                        . '&messages=Ops, Non è stato possibile Cambiare stato al Todo');
                    exit;
                }
            }
            $query->execute();
            header('Location: http://localhost/todolist/index.php?stato=ok&messages=Il Todo è stato aggiornato');
            exit;
        }
    }
}
