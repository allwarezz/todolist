<?php
include_once __DIR__ . '/globals.php';
if (isset($_GET['id'])) {
    \DataHandling\Todo::updateData($_GET);
}

if (isset($_POST['id'])) {
    \DataHandling\Todo::updateData($_POST);
}
