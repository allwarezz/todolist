<?php
namespace DBHandle;

use mysqli;

define("HOST", "localhost");
define("USERNAME", "root");
define("PASS", "");
define("DBNAME", "tododb");
define("TABLE", "todos");
function getConnection()
{
    $mysqli = new mysqli(HOST, USERNAME, PASS, DBNAME);

    if ($mysqli->connect_errno) {
        echo 'Connessione al database fallita: ' . $mysqli->connect_error;
        exit();
    }

    return $mysqli;
}
