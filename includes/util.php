<?php
namespace DataHandling\Utils;

function get_table_head($assoc_array)
{
    $keys = array_keys($assoc_array);
    $html = '';

    foreach ($keys as $key) {
        $html .= '<th scope="col">' . ucwords($key) . '</th>';
    }
    if (!isset($_GET['id'])) {
        $html .= '<th></th>';
    }
    return $html;
}

function get_table_body($items)
{
    $html = '';

    foreach ($items as $row) {
        $html .= '<tr>';
        foreach ($row as $key => $value) {
            if ($key === 'stato') {
                if ($value) {
                    $html .= "<td><a class='link-success text-decoration-none font-weight-bold'"
                        . " alt='Clicca per settarlo ad Incompleto' title='Clicca per settarlo ad Incompleto'"
                        . "  href='/todolist/includes/updateTodo.php?id=" . $row['id'] . "&stato=0'>"
                        . "Completato</a></td>";
                } else {
                    $html .= "<td><a class='link-danger text-decoration-none font-weight-bold'"
                        . " alt='Clicca per Completare' title='Clicca per Completare'"
                        . "  href='/todolist/includes/updateTodo.php?id=" . $row['id'] . "&stato=1'>"
                        . " Non Completato</a></td>";
                }
            } elseif ($key === 'dataCreazione') {
                $date_time = explode(' ', $value);
                $data = implode('/', array_reverse(explode('-', $date_time[0])));
                $html .= "<td>$data</td>";
            } else {
                $html .= "<td>$value</td>";
            }
        }
        $html .= '<td>';
        $html .= '<a class="text-decoration-none" href="/todolist/includes/delTodo.php?id=' . $row['id'] . '">🗑</a>';
        $html .= '<a class="text-decoration-none link-light" href="/todolist/index.php'
            . '?action=loadtodo&id=' . $row['id'] . '">✏</a></td>';
        $html .= '</tr>';
    }

    return $html;
}

function show_alert($state, $message)
{
    if ($state === 'ko') {
        echo '<div class="alert alert-danger" role="alert">' . $message . '</div>';
        return;
    }
    if ($state === 'ok') {
        echo '<div class="alert alert-success" role="alert">' . $message . '</div>';
        return;
    }
}

trait InputSanitize
{
    public static function cleanInput($data)
    {
        $data = trim($data);
        $data = filter_var($data, FILTER_SANITIZE_ADD_SLASHES); // FILTER_SANITIZE_MAGIC_QUOTES
        $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
        return $data;
    }
}
