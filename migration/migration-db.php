<?php
// phpcs:ignoreFile
define("HOST", "localhost");
define("USERNAME", "root");
define("PASS", "");
define("DBNAME", "tododb");
define("TABLE", "todos");
$conn = new mysqli(HOST, USERNAME, PASS);
// Check connection
if ($conn->connect_error) {
    die("Connessione Fallita!\n");
}

// Create database
$sql = "CREATE SCHEMA `" . DBNAME . "` DEFAULT CHARACTER SET utf8mb4 ;";
if ($conn->query($sql) === true) {
    echo "Database " . DBNAME . " Creato!\n";
    $conn->close();
    $conn = new mysqli(HOST, USERNAME, PASS, DBNAME);
    $sql = "CREATE TABLE `" . DBNAME . "`.`" . TABLE . "` (`id` INT NOT NULL AUTO_INCREMENT,"
        . " `testo` VARCHAR(255) NOT NULL,`stato` TINYINT(1) NOT NULL DEFAULT 0,"
        . "  `dataCreazione` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`));";
    echo ($conn->query($sql) === true) ?
    "Tabella " . TABLE . " Creata!\n" :
    "Errore nella creazione della Tabella " . TABLE . "\n";
} else {
    echo "Errore nella creazione del DB";
}

$conn->close();
